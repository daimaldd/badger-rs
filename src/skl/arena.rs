use crate::skl::{Node, OwnedNode};
use std::cmp::Ordering;
use std::default;
use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering::{Acquire, Release};
use std::sync::{Arc, Mutex};

/// `Arena` should be lock-free.
pub struct Arena {
    n: u32,
    buf: Vec<u8>,
}

impl Arena {
    fn new(n: u32) -> Arena {
        let buf = vec![0u8; n as usize];
        Arena { n, buf }
    }

    fn size(&self) -> u32 {
        self.n
    }

    fn reset(&mut self) {
        self.n = 0
    }

    // Returns a pointer to the node located at offset. If the offset is
    // zero, then the null node pointer is returned.
    fn get_node(&self, offset: usize) -> Option<Node> {
        if offset == 0 {
            return None;
        }
        None
    }

    // fn get_value(&self, offset: u32, size: u16) -> OwnedNode {
    //     // let buf = self.buf.lock().unwrap();
    // }
}

#[test]
fn test_arena() {}
