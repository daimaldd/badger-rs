use crate::y::{CAS_SIZE, META_SIZE, USER_META_SIZE};
use byteorder::{BigEndian, LittleEndian};
use byteorder::{ReadBytesExt, WriteBytesExt};
use serde_json::{to_value, Value};
use std::io::Cursor;

/// ValueStruct represents the value info that can be associated with a key, but also the internal
/// Meta field.
/// |meta|user_meta|cas_counter|....|
/// <-----------value--------------->
#[derive(Debug, Clone, Default)]
pub struct ValueStruct {
    value: Vec<u8>,
    meta: u8,
    user_meta: u8,
    cas_counter: u64,
}

impl ValueStruct {
    const VALUE_META_OFFSET: usize = 0;
    const VALUE_USER_META_OFFSET: usize = Self::VALUE_META_OFFSET + META_SIZE;
    const VALUE_CAS_OFFSET: usize = Self::VALUE_USER_META_OFFSET + USER_META_SIZE;
    const VALUE_VALUE_OFFSET: usize = Self::VALUE_CAS_OFFSET + CAS_SIZE;

    pub fn new(value: Vec<u8>, meta: u8, user_meta: u8, cas_counter: u64) -> Self {
        Self {
            value,
            meta,
            user_meta,
            cas_counter,
        }
    }

    /// `encode_size` is the size of the ValueStruct when encoded
    pub fn encode_size(&self) -> usize {
        self.value.len() + Self::VALUE_VALUE_OFFSET
    }

    // value_struct_serialized_size converts a value size to the full serialized size of value + metadata.
    pub fn value_struct_serialized_size(size: u16) -> usize {
        size as usize + Self::VALUE_VALUE_OFFSET
    }
}

/// decode_entries_slice uses the length of the slice to infer the length of the Value field.
impl From<Vec<u8>> for ValueStruct {
    fn from(mut buffer: Vec<u8>) -> Self {
        let mut v = ValueStruct::default();
        v.meta = *buffer.get(Self::VALUE_VALUE_OFFSET).unwrap();
        v.user_meta = *buffer.get(Self::VALUE_META_OFFSET).unwrap();
        v.cas_counter =
            Cursor::new(&mut buffer[Self::VALUE_CAS_OFFSET..(Self::VALUE_CAS_OFFSET + CAS_SIZE)])
                .read_u64::<BigEndian>()
                .unwrap();
        v.value = buffer[Self::VALUE_VALUE_OFFSET..].to_vec();
        v
    }
}

impl From<ValueStruct> for Vec<u8> {
    fn from(v: ValueStruct) -> Self {
        let mut buffer = vec![0u8; v.encode_size()];
        buffer[ValueStruct::VALUE_META_OFFSET] = v.meta;
        buffer[ValueStruct::VALUE_USER_META_OFFSET] = v.user_meta;
        Cursor::new(
            &mut buffer[ValueStruct::VALUE_CAS_OFFSET..(ValueStruct::VALUE_CAS_OFFSET + CAS_SIZE)],
        )
        .write_u64::<BigEndian>(v.cas_counter)
        .unwrap();
        buffer
    }
}

#[test]
fn test_value_struct() {
    let mut v = ValueStruct::new(vec![0u8; 1023], 10, 18, 10);
    let vv: Vec<u8> = v.into();
}
